<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Revel2 - Nuevo Usuario</title>

<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
}
</style>

</head>
<body>


	<jsp:include page="../shared/layout.jsp"></jsp:include>


	<div class="container">
		<div class="d-inline">
			<h1>Nuevo Usuario</h1>
		</div>

		<hr>

		<form:form method="post" action="alta"
			modelAttribute="usuario">
			<form:hidden path="id" />
			<form:hidden path="estado" />
			<form:hidden path="contrasenia" />
			<form:hidden path="cantidadIntentosFallidos" />

			<div class="form-row">

				<div class="form-group col-md-2">
					<form:label path="usuarioId">Usuario </form:label>
					<form:input cssClass="form-control" path="usuarioId" />

				</div>

				<div class="form-group col-md-5">

					<form:label path="nombre">Nombre</form:label>
					<form:input cssClass="form-control" path="nombre" />

				</div>
				<div class="form-group col-md-5">
					<form:label path="apellido">Apellido</form:label>
					<form:input cssClass="form-control" path="apellido" />
				</div>


			</div>


			<div class="form-row">


				<div class="form-group col-md-7">

					<form:label path="mail">Mail</form:label>
					<form:input cssClass="form-control" path="mail" />
				</div>

				<div class="form-group col-md-5">

					<form:label path="rol">Rol</form:label>
					<form:select cssClass="form-control" path="rol">
						<form:option value="" label="Seleccione un Rol"></form:option>
						<form:options items="${listaRoles }"></form:options>


					</form:select>
				</div>
			</div>
			<hr>
			<h3>Usuarios Vinculados</h3>


			<div class="form-row">


				<c:forEach items="${listaMarcas }" var="usrAdm" varStatus="status">
					<%-- 					<form:hidden path="usuariosMarca[${status.index }].id" /> --%>


					<form:hidden path="usuariosMarca[${status.index }].marca" />

					<form:hidden path="usuariosMarca[${status.index }].usuario" />

					<div class="form-group col-md-2">
						<form:label path="usuariosMarca[${status.index }].marca">

							<span class="badge badge-primary">${usrAdm}</span>
						</form:label>
						<form:input cssClass="form-control"
							path="usuariosMarca[${status.index }].usuarioMarca" />

					</div>

				</c:forEach>


			</div>


			<hr>


				<form:errors path="usuarioId" cssClass="alert alert-danger"></form:errors>
				<form:errors path="nombre" cssClass="alert alert-danger"></form:errors>
				<form:errors path="apellido" cssClass="alert alert-danger"></form:errors>

			<div class="form-group">

				<input type="submit" value="Confirmar" class="btn btn-primary" /> <input
					type="button" onclick="history.back()" class="btn btn-secondary"
					value="Volver">

			</div>


		</form:form>


	</div>

</body>
</html>