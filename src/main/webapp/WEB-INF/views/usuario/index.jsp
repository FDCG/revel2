<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${spring.application.name } - Usuarios</title>


</head>
<body>


	<jsp:include page="../shared/layout.jsp"></jsp:include>
	
	<c:set var="contextPath" value="${pageContext.request.contextPath}"/>	


	<div class="container">

		<h1>Usuarios 
		
		<a href="${contextPath}/usuarios/alta"><button class="btn btn-primary" style="float: right;">Alta Nuevo Usuario</button> </a>

		</h1>
		<hr>
		<table class="table table-hover table-dark table-bordered">
			<thead>
				<tr>
					<th scope="col" class="text-center">Id</th>
					<th scope="col">Usuario</th>
					<th scope="col">Apellido</th>
					<th scope="col">Nombre</th>
					<th scope="col">Usuarios Vinculados</th>
					<th scope="col" class="text-center">Rol</th>
					<th scope="col" class="text-center">Estado</th>
					<th scope="col" class="text-center">Editar</th>

				</tr>
			</thead>
			<tbody>
				<c:forEach items="${usuarios }" var="usuario">
					<tr>
						<th scope="row" class="text-center">${usuario.getId() }</th>
						<td>${usuario.getUsuarioId() }</td>
						<td>${usuario.getApellido() }</td>
						<td>${usuario.getNombre() }</td>

						<td>${usuario.getUsuariosMarca().size() > 0 ? usuario.usuariosMarca.toString() : '' }</td>
						<td class="text-center"><span class="badge badge-pill badge-light">${usuario.getRol() }</span></td>
						<td class="text-center"><c:choose>
								<c:when test="${usuario.getEstado() == 'Activo' }">
									<span class="badge badge-success">
								</c:when>
								<c:when test="${usuario.getEstado() == 'Inactivo' }">
									<span class="badge badge-secondary">
								</c:when>
								<c:otherwise>
									<span class="badge badge-danger">
								</c:otherwise>

							</c:choose> ${usuario.getEstado() }</span></td>

						<td class="text-center"><a href="${contextPath}/usuarios/editar/?id=${usuario.getId()}"><i class="far fa-edit"></i></a></td>
					</tr>
				</c:forEach>


			</tbody>
		</table>

	</div>

</body>
</html>