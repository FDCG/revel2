<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Revel2 - Editar Usuario</title>


</head>
<body>


	<jsp:include page="../shared/layout.jsp"></jsp:include>


	<div class="container">
		<div class="d-inline">
			<h1>
				Editar Usuario: ${usuario.getUsuarioId()} <span
					style="float: right;" class="badge badge-info">${usuario.getEstado() }</span>
			</h1>
		</div>

		<hr>

		<form:form method="post" action="/usuarios/editar"
			modelAttribute="usuario">
			<form:hidden path="id" />
			<form:hidden path="estado" />
			<form:hidden path="contrasenia" />
			<form:hidden path="cantidadIntentosFallidos" />
			<div class="form-row">

				<div class="form-group col-md-2">
					<form:label path="usuarioId">Usuario</form:label>
					<form:input cssClass="form-control" path="usuarioId" />
				</div>

				<div class="form-group col-md-5">

					<form:label path="nombre">Nombre</form:label>
					<form:input cssClass="form-control" path="nombre" />

				</div>
				<div class="form-group col-md-5">
					<form:label path="apellido">Apellido</form:label>
					<form:input cssClass="form-control" path="apellido" />
				</div>


			</div>


			<div class="form-row">


				<div class="form-group col-md-7">

					<form:label path="mail">Mail</form:label>
					<form:input cssClass="form-control" path="mail" />
				</div>

				<div class="form-group col-md-5">

					<form:label path="rol">Rol</form:label>
					<form:select cssClass="form-control" path="rol"
						items="${listaRoles}">
						<form:options itemValue="${usuario.getRol() }" />
					</form:select>
				</div>
			</div>
			<hr>
			<h3>Usuarios Vinculados</h3>


			<div class="form-row">


				<c:forEach items="${usuario.getUsuariosMarca() }" var="usrAdm"
					varStatus="status">
					<form:hidden path="usuariosMarca[${status.index }].id" />
					<form:hidden path="usuariosMarca[${status.index }].marca" />
					<form:hidden path="usuariosMarca[${status.index }].usuario" />

					<div class="form-group col-md-2">
						<form:label path="usuariosMarca[${status.index }].marca">

							<span class="badge badge-primary">${usrAdm.getMarca() }</span>
						</form:label>
						<form:input cssClass="form-control"
							path="usuariosMarca[${status.index }].usuarioMarca" />

					</div>

				</c:forEach>


			</div>
			<hr>

			<div class="form-group">

				<input type="submit" value="Guardar" class="btn btn-primary" /> <input
					type="button" onclick="history.back()" class="btn btn-secondary"
					value="Volver"> <input style="float: right;" type="button"
					onclick="cambiarEstado(${usuario.getId()})"
					<c:choose>
						<c:when test="${usuario.getEstado() == 'Inhabilitado' }">
						 class="btn btn-success" value="Rehabilitar"
						</c:when>
						<c:otherwise>
						class="btn btn-danger" value="Inhabilitar"
						</c:otherwise>
					</c:choose> />



			</div>


		</form:form>


	</div>

</body>
</html>