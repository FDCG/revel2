package com.gestionap.revel2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gestionap.revel2.model.Usuario;

public interface IUsuarioRepository extends JpaRepository<Usuario, Integer> {
	
	

}
