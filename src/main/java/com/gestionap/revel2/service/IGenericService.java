package com.gestionap.revel2.service;

import java.util.List;

public interface IGenericService<T> {
	
	public void create(T entity);
	public void delete (int id);
	public List<T> getAll();
	public T getById(int id);
	public void update(T entity);
	
}
