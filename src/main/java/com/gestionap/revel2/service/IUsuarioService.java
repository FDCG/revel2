package com.gestionap.revel2.service;

import com.gestionap.revel2.model.Usuario;

public interface IUsuarioService extends IGenericService<Usuario>  {	

}
