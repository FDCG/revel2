package com.gestionap.revel2.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.gestionap.revel2.model.Usuario;
import com.gestionap.revel2.repository.IUsuarioRepository;

@Service
public class UsuarioService implements IUsuarioService {

	private IUsuarioRepository usuarioRepository;
	
	public UsuarioService (IUsuarioRepository usuarioRepository) {
		this.setUsuarioRepository(usuarioRepository);
	}
	
	@Override
	public void create(Usuario entity) {
		this.usuarioRepository.save(entity);		
	}

	@Override
	public void delete(int id) {
		this.usuarioRepository.deleteById(id);
	}

	@Override
	public List<Usuario> getAll() {
		return this.usuarioRepository.findAll();
	}

	@Override
	public Usuario getById(int id) {
		return this.usuarioRepository.getOne(id);
	}

	@Override
	public void update(Usuario entity) {
		this.usuarioRepository.save(entity);
	}

	public IUsuarioRepository getUsuarioRepository() {
		return usuarioRepository;
	}

	public void setUsuarioRepository(IUsuarioRepository usuarioRepository) {
		this.usuarioRepository = usuarioRepository;
	}


}
