package com.gestionap.revel2.controller;

import java.lang.reflect.Array;
import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.util.EnumValues;
import com.gestionap.revel2.model.Administradora;
import com.gestionap.revel2.model.EstadoUsuario;
import com.gestionap.revel2.model.RolUsuario;
import com.gestionap.revel2.model.Usuario;
import com.gestionap.revel2.service.UsuarioService;

@Controller
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	UsuarioService usuarioService;
	
	
	@GetMapping()
	public ModelAndView getUsuarios() {
		
		ArrayList<Usuario> usuarios = (ArrayList<Usuario>) usuarioService.getAll();
		
		return new ModelAndView("usuario/index", "usuarios", usuarios);
	}
	
	@ModelAttribute("usuario")
	@RequestMapping(value = "/editar", params = "id", method = RequestMethod.GET)
	public ModelAndView editarUsuario(@RequestParam("id") int id) {
		
		ModelAndView model = new ModelAndView();
		
		model.addObject("listaRoles", RolUsuario.values());
		
		model.addObject("listaMarcas", Administradora.values());
		
		Usuario usuario = usuarioService.getById(id);
		
		model.addObject("usuario", usuario);
		
		model.setViewName("usuario/editar");
		
	
		return model;
	}
	
	@RequestMapping(value = "/editar", method = RequestMethod.POST)
	public ModelAndView editarUsuario(@Valid @ModelAttribute("usuario")Usuario usuario, 
			BindingResult result, ModelMap model) {
		

        if (result.hasErrors()) {
            return new ModelAndView("usuarios", model);
        }
        
        ModelAndView mav = new ModelAndView();
        
        mav.addObject("usuario", usuario);
        
        usuarioService.update(usuario);
		
		mav.setViewName("redirect:/usuarios");
        
        return mav;
	}
	
	@ModelAttribute("usuario")
	@RequestMapping(value = "/alta", method = RequestMethod.GET)
	public ModelAndView altaUsuario() {
		
		ModelAndView model = new ModelAndView();
		
		model.addObject("listaRoles", RolUsuario.values());
		
		model.addObject("listaMarcas", Administradora.values());
		
		Usuario usuario = new Usuario();
		
		usuario.setCantidadIntentosFallidos(0);
		usuario.setContrasenia("NuevoIngreso");
		usuario.setEstado(EstadoUsuario.Activo);

		model.addObject("usuario", usuario);
		
		model.setViewName("usuario/alta");
		
		return model;
	}
	
	
	@RequestMapping(value = "/alta", method = RequestMethod.POST)
	public ModelAndView altaUsuario(@Validated @ModelAttribute("usuario")Usuario usuario, 
			BindingResult result, ModelMap model) {
		

        if (result.hasErrors()) {
        	model.addAttribute("usuario", usuario);
            return new ModelAndView("usuario/alta", model);
        }
        
        ModelAndView mav = new ModelAndView();
        
        mav.addObject("usuario", usuario);
        
        usuario.setUsuarioEnUsuariosMarca(usuario.getUsuariosMarca());
        
        usuarioService.create(usuario);
		
		mav.setViewName("redirect:/usuarios");
        
        return mav;
	}
	
}
