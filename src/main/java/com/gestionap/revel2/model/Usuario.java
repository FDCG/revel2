package com.gestionap.revel2.model;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Usuario {
	

	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	

	@Size(min=8, max=8)
	@NotNull
	private String usuarioId;
	
	@Size(min=6, max=30)
	@NotNull
	private String contrasenia;
	
	@Size(min=3, max=50)
	@NotNull
	private String nombre;
	
	@Size(min=2, max=50)
	@NotNull
	private String apellido;
	
	@Email
	@NotNull
	private String mail;
	
	
	@OneToMany(targetEntity = UsuarioMarca.class, mappedBy = "usuario", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<UsuarioMarca> usuariosMarca;
	
	private int cantidadIntentosFallidos;
	
	private EstadoUsuario estado;
	
	private RolUsuario rol;

	
	
	public Usuario(String usuarioId, String contrasenia, String nombre, String apellido, String mail,
			List<UsuarioMarca> usuariosMarca, int cantidadIntentosFallidos, EstadoUsuario estado, RolUsuario rol) {
		super();
		this.usuarioId = usuarioId;
		this.contrasenia = contrasenia;
		this.nombre = nombre;
		this.apellido = apellido;
		this.mail = mail;
		this.usuariosMarca = usuariosMarca;
		this.cantidadIntentosFallidos = cantidadIntentosFallidos;
		this.estado = estado;
		this.rol = rol;
	}


	public Usuario() {
		super();
		this.usuariosMarca = new ArrayList<UsuarioMarca>();
		setInitialUsuarioMarca(this.usuariosMarca, this);
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getUsuarioId() {
		return usuarioId;
	}


	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}


	public String getContrasenia() {
		return contrasenia;
	}


	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public List<UsuarioMarca> getUsuariosMarca() {
		return usuariosMarca;
	}


	public void setUsuariosMarca(List<UsuarioMarca> usuariosMarca) {
		this.usuariosMarca = usuariosMarca;
	}


	public int getCantidadIntentosFallidos() {
		return cantidadIntentosFallidos;
	}


	public void setCantidadIntentosFallidos(int cantidadIntentosFallidos) {
		this.cantidadIntentosFallidos = cantidadIntentosFallidos;
	}


	public EstadoUsuario getEstado() {
		return estado;
	}


	public void setEstado(EstadoUsuario estado) {
		this.estado = estado;
	}
	
	private void setInitialUsuarioMarca(List<UsuarioMarca> usuariosMarca, Usuario usuario) {
		for (int i = 0; i < Administradora.values().length; i++) {
			usuariosMarca.add(new UsuarioMarca(Administradora.values()[i], usuario));
		}
	}
	
	public void setUsuarioEnUsuariosMarca(List<UsuarioMarca> usuariosMarca) {
		for (UsuarioMarca usuarioMarca : usuariosMarca) {
			usuarioMarca.setUsuario(this);
		}
	}


	public RolUsuario getRol() {
		return rol;
	}


	public void setRol(RolUsuario rol) {
		this.rol = rol;
	}


	@Override
	public String toString() {
		return "Usuario [id=" + id + ", usuarioId=" + usuarioId + ", contrasenia=" + contrasenia + ", nombre=" + nombre
				+ ", apellido=" + apellido + ", mail=" + mail + ", usuariosMarca=" + usuariosMarca
				+ ", cantidadIntentosFallidos=" + cantidadIntentosFallidos + ", estado=" + estado + ", rol=" + rol + "]";
	}



	


	
	

}
