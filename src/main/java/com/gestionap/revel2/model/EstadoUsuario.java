package com.gestionap.revel2.model;

public enum EstadoUsuario {
Activo, Bloqueado, Inhabilitado, Inactivo
}
