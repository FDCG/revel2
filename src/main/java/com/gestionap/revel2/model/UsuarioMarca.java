package com.gestionap.revel2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class UsuarioMarca{
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY) @Column(name="Id")
	private int id;
	private String usuarioMarca;
	private Administradora marca;
	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "usuario_id", insertable = true)
	private Usuario usuario;
	
	
	public UsuarioMarca(String usuarioMarca, Administradora marca) {
		super();
		this.usuarioMarca = usuarioMarca;
		this.marca = marca;
	}
	
	public UsuarioMarca(Administradora marca) {
		super();
		this.marca = marca;

	}
	
	public UsuarioMarca(Administradora marca, Usuario usuario) {
		super();
		this.marca = marca;
		this.usuario = usuario;

	}
	
	
	public UsuarioMarca() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Administradora getMarca() {
		return marca;
	}

	public void setMarca(Administradora marca) {
		this.marca = marca;
	}

	public String getUsuarioMarca() {
		return usuarioMarca;
	}

	public void setUsuarioMarca(String usuarioMarca) {
		this.usuarioMarca = usuarioMarca;
	}

	@Override
	public String toString() {

		return usuarioMarca!=null ? usuarioMarca : "";
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}


	
}

